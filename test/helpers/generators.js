'use strict';

import { v4 as uuidv4 } from 'uuid';
import { mailHost, standardPasswordAlt } from '../config/envconfig.js';
import { standardPassword } from '../config/envconfig.js';
import * as generator from 'generate-password';
import * as mm from './magicmisc.js';

function generateUsername(uuid){
  let u = uuid.substring(1,20);
  return u;
}

function generateEmail(uuid){
  let e = uuid + '@' + mailHost;
  return e;
}

function generateValidPassword(){
  let p = generator.generate({
    length: 8,
    uppercase: true,
    numbers: true,
    symbols: true,
    strict: true,
  });
  return p;
}

export function generateUuid(){
  return uuidv4();
}

export function generateUserData(){
  let uuid = generateUuid();
  let mobile = '020' + mm.getRandomInt(10000000, 99999999);
  let userdata = {
    'guid': uuid,
    'username': generateUsername(uuid),
    'email': generateEmail(uuid),
    'password': standardPassword,
    'password_alt': standardPasswordAlt,
    'password_random_valid': generateValidPassword(),
    'mobile': mobile,
  };
  return userdata;
}