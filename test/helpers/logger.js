'use strict';

import winston from 'winston';
import { loggerSettings } from '../config/envconfig.js';

//Mind this one: https://stackoverflow.com/questions/51012150/winston-3-0-colorize-whole-output-on-console

// Mind this one: https://github.com/winstonjs/winston/issues/1272
// Switching json on/off does not help, use this ^^^ solution instead
// to switch back to json, comment out printf line

const OPTIONS = {
  file: {
    level: loggerSettings.file.loglevel,
    filename: loggerSettings.file.filename,
    handleExceptions: loggerSettings.file.handleExceptions,
    json: loggerSettings.file.json,
    maxsize: loggerSettings.file.maxsize,
    maxFiles: loggerSettings.file.maxFiles,
    colorize: loggerSettings.file.colorize,
    timestamp: loggerSettings.file.timestamp,
  },
  console: {
    level: loggerSettings.console.loglevel,
    handleExceptions: loggerSettings.console.handleExceptions,
    json: loggerSettings.console.json,
    colorize: loggerSettings.console.colorize,
    timestamp: loggerSettings.console.timestamp,
  },
};

winston.addColors({
  error: loggerSettings.color.error,
  warn: loggerSettings.color.warn,
  info: loggerSettings.color.info,
  debug: loggerSettings.color.debug,
  silly: loggerSettings.color.silly,
});

let logger;

// if block is needed to work around the weird and easy to google bug in winston that sends special symbols to the log
if (loggerSettings.color.switch === 'true') { 
  logger = winston.createLogger({
    levels: winston.config.npm.levels,
    format: winston.format.combine(
      winston.format.timestamp(),
      winston.format.prettyPrint(),
      // next one send extra spec symbols to text file
      winston.format.colorize( { all: true } ),
      winston.format.printf(i => `${i.timestamp} | ${i.message}`),
    ),
    transports: [
      new winston.transports.File(OPTIONS.file),
      new winston.transports.Console(OPTIONS.console),
    ],
    exitOnError: false,
  });
} else {
  logger = winston.createLogger({
    levels: winston.config.npm.levels,
    format: winston.format.combine(
      winston.format.timestamp(),
      winston.format.prettyPrint(),
      winston.format.colorize(),
      winston.format.printf(i => `${i.timestamp} | ${i.message}`),
    ),
    transports: [
      new winston.transports.File(OPTIONS.file),
      new winston.transports.Console(OPTIONS.console),
    ],
    exitOnError: false,
  });
}

export default logger;