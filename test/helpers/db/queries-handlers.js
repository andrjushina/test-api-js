'use strict';

import logger from '../logger.js';
import * as mm from '../magicmisc.js';

export function getFirstRowFromResultSet(resultSet) {
  let firstRaw = resultSet.rows[0];
  return firstRaw;
}

export function getRandomValueFromResultSet(resultSet, columnName) {
  logger.silly( 'Query result set is:  ' + JSON.stringify(resultSet));
  logger.debug( 'Query row count is:  ' + resultSet.rowCount);
  let r = mm.getRandomInt(0, resultSet.rowCount-1);
  logger.silly( 'Random value is:  ' + r);
  let selectedRow = resultSet.rows[r];
  let randomValue;
  try {
    randomValue = selectedRow[columnName];
  } catch (e) {
    logger.error( 'There was en error:  ' + e);
  }
  return randomValue;
}
