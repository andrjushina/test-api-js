'use strict';

import pg from 'pg';
import logger from '../logger.js';

const { Client } = pg;

// source: https://github.com/brianc/node-postgres/wiki/FAQ

/**
 * Creates a DB client
 * @param connectionString 
 * @returns a DB client (no connection)
 */
function getDbClient(connectionString) {
  const client = new Client({
    connectionString,
  });
  logger.silly( 'Client:  ' + String(client));
  return client;
}

/**
 * Opens DB connection or returns an error if it fails
 * @param connectionString is connection string that comes from testlab.js, usually, tl.currConnString
 * @returns open DB connection
 */
export async function clientConnect(connectionString) {
  let client;
  try{
    client = getDbClient(connectionString);
    await client.connect();
    logger.silly('Connection is open');
  } catch (e) {
    logger.error( 'Error message:  ' + e);
  }
  return client;
}

/**
 * Closes DB connection or returns an error it the action fails
 * @param client is a DB client created earlier
 * @returns closed DB connection
 */
export async function clientDisconnect(client) {
  try {
    await client.end();
    logger.silly('Connection is closed');
  } catch (e) {
    logger.error( 'There was an error:  ' + e);
  }
}