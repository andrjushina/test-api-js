'use strict';

import dateFormat, { masks } from 'dateformat ';
import logger from './logger.js';

function getFormattedDate(date, formatString) {
  let formattedDate = dateFormat(date, formatString);
  logger.debug('Formatted date is like this: ' + formattedDate);
  return formattedDate;
}