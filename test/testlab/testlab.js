'use strict';

import fetch from 'node-fetch';
import * as ec from '../config/envconfig.js';
import { generateUserData } from '../helpers/generators.js';
import logger from '../helpers/logger.js';

/**
* This file configures runtime data throughout the solution.
* To do so it combines data setup by propreaderr and envconfig.
* It also defines methods and variables that are heavily used in tests.
* Please note that service-, case- and suite-specific methods and variables
* are configured via testlab/api-object/<service.name> folder
*/

/**
 * 
 * @param {*} dbType 
 * @param {*} username 
 * @param {*} password 
 * @param {*} dbHost 
 * @param {*} dbPort 
 * @param {*} dbName 
 * @returns 
 */
function buildConnectionString(dbType, username, password, dbHost, dbPort, dbName) {
  let connectionString = dbType + '://' + username + ':' + password + '@' + dbHost + ':' + dbPort + '/' + dbName;
  logger.debug('Connection string is set to: ' + connectionString);
  return connectionString;
}

/**
 * 
 * 
 * @param {*} url 
 * @param {*} method 
 * @param {*} headers 
 * @param {*} body 
 * @returns 
 */
async function fetchResponse(url, method, headers, body){
  let response;
  try {
    response = fetch(url, {
      method: method,
      headers: headers,
      body: JSON.stringify(body) });
    return await response;
  } catch(err){
    logger.error('There was an error: ' + err);
    logger.error('Here is the original response: ' + (await response).text());
  }
}

/**
 * 
 * 
 * @param {*} url 
 * @param {*} method 
 * @param {*} headers 
 * @param {*} body 
 * @returns 
 */
async function fetchResponseRawBody(url, method, headers, body){
  let response;
  try {
    response = fetch(url, {
      method: method,
      headers: headers,
      body: body });
    return await response;
  } catch(err){
    logger.error('There was an error: ' + err);
    logger.error('Here is the original response: ' + (await response).text());
  }
}

/**
 * 
 * @param {*} url 
 * @param {*} method 
 * @param {*} headers 
 * @param {*} body 
 * @returns 
 */

function setRequestData(url, method, headers, body){
  let td = {
    'url': url,
    'method': method,
    'headers': headers,
    'body': body };
  return td;
}

/**
 * 
 * 
 * @param {*} schema 
 * @param {*} url 
 * @param {*} port 
 * @param {*} path 
 * @param {*} parameters 
 * @returns 
 */
function buildUrl(schema, url, port, path, parameters){
  let fullUrl;
  if (path!==null && parameters===null && port==='' ) {
    fullUrl = schema + url + path;
  } else if (path!==null && parameters!==null && port==='') {
    fullUrl = schema + url + path + parameters;
  } else if (path!==null && parameters===null && port!=='') {
    fullUrl = schema + url + ':' + port + path;
  } else if (path!==null && parameters!==null && port!=='') {
    fullUrl = schema + url + ':' + port + path + parameters;
  } else if (path===null && parameters===null && port==='' ) {
    fullUrl = schema + url;
  } else if (path===null && parameters!==null && port==='') {
    fullUrl = schema + url + parameters;
  } else if (path===null && parameters===null && port!=='') {
    fullUrl = schema + url + ':' + port;
  } else if (path===null && parameters!==null && port!=='') {
    fullUrl = schema + url + ':' + port + parameters;
  } else {
    logger.error('Error: you are using an unsupported configuration of URL, please support it and be happy');
  }
  return fullUrl;
}

// EXPORTED (PUBLIC BELOW THIS LINE)

// we are using generators directly from the suite config files
//export let uniqueUserData = generateUserData();

export let currEnv = ec.configuredEnv;
export let currUsers = ec.configuredUsers;
export let currTestData = ec.configuredTestData;
export let currConnString = buildConnectionString(
  ec.configuredEnv.db.db_type,
  ec.configuredEnv.db.db_user,
  ec.configuredEnv.db.db_password,
  ec.configuredEnv.db.db_host,
  ec.configuredEnv.db.db_port,
  ec.configuredEnv.db.db_name,
);

export let mochaTimeout = currEnv.mocha_request_timeout;

export let pathTafRepo = ec.getTafPath();
export let pathDataImages = pathTafRepo + ec.configuredTestData.paths.images;
export let pathDataVideos = pathTafRepo + ec.configuredTestData.paths.videos;


// Working with requests

export async function makeRequest(path = null, parameters = null, method, headers, body) {
  logger.debug('Beginning request configuration');
  let url = buildUrl(currEnv.schema, currEnv.url, currEnv.port, path, parameters);
  let requestData = setRequestData(url, method, headers, body);
  logger.debug('Actual request is to be sent like this: ' + getRequestAsPrettyString(requestData));
  return await fetchResponse(requestData.url, requestData.method, requestData.headers, requestData.body);
}

export function getRequestAsPrettyString(request){
  return JSON.stringify(request, null, 2);
}

// Working with responses

export function getResposeAsPrettyString(response){
  return JSON.stringify(response, null, 2);
}

// DEPRECATED BELOW THIS LINE

/**
 * 
 * @deprecated
 * 
 * @param {*} url 
 * @param {*} method 
 * @param {*} headers 
 * @param {*} body 
 * @returns 
 */
async function fetchResponseAsJson(url, method, headers, body){
  let response;
  try {
    response = fetch(url, {
      method: method,
      headers: headers,
      body: JSON.stringify(body) });
    return (await response).json();
  } catch(err){
    logger.error('There was an error: ' + err);
    logger.error('Here is the original response: ' + (await response).text());
  }
}

/**
 * 
 * @deprecated
 * 
 * @param {*} url 
 * @param {*} method 
 * @param {*} headers 
 * @param {*} body 
 * @returns 
 */
async function fetchResponseRawBodyAsJson(url, method, headers, body){
  let response;
  try {
    response = fetch(url, {
      method: method,
      headers: headers,
      body: body });
    return (await response).json();
  } catch(err){
    logger.error('There was an error: ' + err);
    logger.error('Here is the original response: ' + (await response).text());
  }
}