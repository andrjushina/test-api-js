'use strict';

import dotenv from 'dotenv';

// no logger here, console only

const DOTENV_CONFIG = dotenv.config();
export const ENV_FILE_CONTENT = DOTENV_CONFIG.parsed;