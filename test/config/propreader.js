'use strict';

import fs from 'fs';
import path from 'path';
import jp from 'jsonpath';

/**
* This file configures properties throughout the solution
* Base files are properties.json and files from testlab/testdata folder
*/

// INFO: No logger usage here, console.log only to avoid cyclic init

const __dirname = path.resolve();

const PROPERTY_PATH = './test/config/properties.json';
const PROPERTY_FILE_ENCODING = 'utf8';

const USER_DATA_PATH = './test/testlab/testdata-common/users.json';
const USER_DATA_FILE_ENCODING = 'utf-8';

const DATA_SETTINGS_PATH = './test/testlab/testdata-common/data-settings.json';
const DATA_SETTINGS_FILE_ENCODING = 'utf-8';


export function configureSuitePath(suiteName) {
  let suitePath = {
    'suiteConfigPath': './test/tests/' + suiteName + '/suitedata.json',
    'suiteConfigFileEncoding': 'utf8',
  };
  return suitePath;
}

export function getAllProperties(){
  let all_properties = fs.readFileSync(path.resolve(__dirname, PROPERTY_PATH), PROPERTY_FILE_ENCODING);
  return all_properties;
}

export function getAllUsers(){
  let all_users = fs.readFileSync(path.resolve(__dirname, USER_DATA_PATH), USER_DATA_FILE_ENCODING);
  return all_users;
}

export function getAllDataSettings(){
  let all_users = fs.readFileSync(path.resolve(__dirname, DATA_SETTINGS_PATH), DATA_SETTINGS_FILE_ENCODING);
  return all_users;
}

export function getPropertybyName(property_name){
  let all_properties = getAllProperties();
  let value = jp.query(JSON.parse(all_properties), property_name).pop();
  return value;
}

export function getUsersForEnv(env){
  let all_users = getAllUsers();
  let value = jp.query(JSON.parse(all_users), env).pop();
  return value;
}

export function getDataSettings(){
  let all_data = getAllDataSettings();
  let value = JSON.parse(all_data);
  return value;
}

export function getDataSettingsbyEntityType(entity_type){
  let all_data = getAllDataSettings();
  let value = jp.query(JSON.parse(all_data), entity_type).pop();
  return value;
}