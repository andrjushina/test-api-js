'use strict';

import * as propreader from './propreader.js';
import * as envreader from './envreader.js';
import jp from 'jsonpath';
import path from 'path';

/**
* This file configures environment data throughout the solution,
* and ensures switching between them and supported Git branches
* Base files are properties.json and files from testlab/testdata folder
*/

// INFO: No logger usage here, console.log only to avoid cyclic init

const __dirname = path.resolve();

/**
 * Analyses environment variables for BACKEND_ENV value.
 * Values 'FEA', 'FIX' and 'REF' are configured at Jenkins level.
 * FEA stands for 'feature', FIX for 'fix' & REF for 'refactoring'
 * Related methods: getCurrentEnv(), getCurrentEnvDetails(envInfo)
 * @returns String with env value or undefined if in .env file BACKEND_ENV is not set.
 */
function getBackEndEnv() {
  let env;
  const regexp = new RegExp('FEA|FIX|REF');
  if (process.env.BACKEND_ENV !== undefined && process.env.BACKEND_ENV !== '' && regexp.test(process.env.BACKEND_ENV)) {
    env = 'int';
  } else {
    env = process.env.BACKEND_ENV;
  }
  return env;
}

/**
 * Primary method for defining selected back-end environment.
 * @returns String carrying final env value, such as 'local', 'int' or 'uat'
 */
function getCurrentEnv() {
  let se;
  if (getBackEndEnv() !== undefined && getBackEndEnv() !== '') {
    se = getBackEndEnv().toLowerCase();
  } else {
    se = propreader.getPropertybyName('$.selected_env');
  }
  return se;
}

/**
 * Gets from properties.json configuration block for a given env value
 * @param {*} envInfo is a string returned by getCurrentEnv() & getBackEndEnv()
 * @returns json object with current env settings
 */
function getCurrentEnvDetails(envInfo) {
  let e;

  if (envInfo === 'local') {
    e = propreader.getPropertybyName('$.environments.local');
  } else if (envInfo === 'int') {
    e = propreader.getPropertybyName('$.environments.int');
  } else if (envInfo === 'uat') {
    e = propreader.getPropertybyName('$.environments.uat');
  } else {
    console.log('Could not find env value, assigning default one: int');
    e = propreader.getPropertybyName('$.environments.int');
  }
  e = replacePlaceholders(e, envInfo);
  return e;
}

/**
 * Gets mail host info from config/properties.json
 * Mail host is a host of a service for free-of-charge temp emails
 * @returns string with mail host address
 */
function getMailHost() {
  let mh = propreader.getPropertybyName('$.generator.mailhost');
  return mh;
}

/**
 * Gets default password from config/properties.json for use with test data
 * @returns string with a standard password
 */
function getStandardPassword() {
  let sp = propreader.getPropertybyName('$.generator.standard_pass');
  return sp;
}

/**
 * Gets default password from config/properties.json for use with test data
 * @returns string with an alternative standard password
 */
function getStandardPasswordAlt() {
  let sp = propreader.getPropertybyName('$.generator.standard_pass_alt');
  return sp;
}

/**
 * Get logger settings from config/properties.json
 * @returns json object with logger settings for further processing in logger.js
 */
function getLoggerSettings() {
  let ls = propreader.getPropertybyName('$.logger');
  return ls;
}

/**
 * Gets users information for the given environment from config/testdata/users.json
 * @param {*} envInfo is a string returned by getCurrentEnv() & getBackEndEnv()
 * @returns json object with static users for the selected environment
 */
function getUsersForEnv(envInfo) {
  let u;

  if (envInfo == 'local') {
    u = propreader.getUsersForEnv('$.local');
  } else if (envInfo == 'int') {
    u = propreader.getUsersForEnv('$.int');
  } else if (envInfo == 'uat') {
    u = propreader.getUsersForEnv('$.uat');
  } else {
    console.log('Could not find users value, assigning default one: int');
    u = propreader.getUsersForEnv('$.int');
  }
  return u;
}

// eslint-disable-next-line no-unused-vars
/**
 * Replaces placeholders and current values in config/properties.json with actual values from .env file
 * @param {*} rawData is a subset of data from config/properties.json $.environments.<name>
 * @param {*} envInfo is a string returned by getCurrentEnv() & getBackEndEnv()
 * @returns json object with placeholders replaced by values from .env file, without configured .env file authentication with social networks won't work
 */
function replacePlaceholders(rawData, envInfo) {
  let processedData;
  let envInfoUpper = envInfo.toUpperCase();

  if (rawData !== undefined) {

    // basic set

    rawData.schema = assignIfNotNull(rawData.schema, getPropertyFromEnvByName(envInfoUpper + '_SCHEMA'));
    rawData.url = assignIfNotNull(rawData.url, getPropertyFromEnvByName(envInfoUpper + '_URL'));
    rawData.port = assignIfNotNull(rawData.port, getPropertyFromEnvByName(envInfoUpper + '_PORT'));
    rawData.mocked = assignIfNotNull(rawData.mocked, getPropertyFromEnvByName(envInfoUpper + '_MOCKED'));
    rawData.mocha_request_timeout = assignIfNotNull(rawData.mocha_request_timeout, getPropertyFromEnvByName(envInfoUpper + '_MOCHA_REQUEST_TIMEOUT'));

    // db

    rawData.db.db_type = assignIfNotNull(rawData.db.db_type, getPropertyFromEnvByName(envInfoUpper + '_DB_TYPE'));
    rawData.db.db_user = assignIfNotNull(rawData.db.db_user, getPropertyFromEnvByName(envInfoUpper + '_DB_USER'));
    rawData.db.db_password = assignIfNotNull(rawData.db.db_password, getPropertyFromEnvByName(envInfoUpper + '_DB_PASSWORD'));
    rawData.db.db_host = assignIfNotNull(rawData.db.db_host, getPropertyFromEnvByName(envInfoUpper + '_DB_HOST'));
    rawData.db.db_port = assignIfNotNull(rawData.db.db_port, getPropertyFromEnvByName(envInfoUpper + '_DB_PORT'));
    rawData.db.db_name = assignIfNotNull(rawData.db.db_name, getPropertyFromEnvByName(envInfoUpper + '_DB_NAME'));

    // aws
    rawData.clouds.aws_host = assignIfNotNull(rawData.clouds.aws_host, getPropertyFromEnvByName(envInfoUpper + '_AWS_HOST'));
    rawData.clouds.aws_s3_temp_bucket = assignIfNotNull(rawData.clouds.aws_s3_temp_bucket, getPropertyFromEnvByName(envInfoUpper + '_AWS_S3_TEMP_BUCKET'));
    rawData.clouds.aws_region = assignIfNotNull(rawData.clouds.aws_region, getPropertyFromEnvByName(envInfoUpper + '_AWS_REGION'));
    
    processedData = rawData;

  } else {

    console.log('Source properties.json is empty. Assigning data as it is.');
    processedData = rawData;

  }

  return processedData;
}

/**
 * Processes property information that comes from .env file
 * @param {*} propertyName is a value that comes from .env file
 * @returns gets property value from .env file
 */
function getPropertyFromEnvByName(propertyName) {
  let value;
  if (envreader.ENV_FILE_CONTENT !== undefined) {
    value = jp.query(envreader.ENV_FILE_CONTENT, propertyName).pop();
  } else {
    value = 'ENV_FILE_NOT_AVAILABLE';
  }
  return value;
}

/**
 * Assigns a value if incoming is neither undefined nor null
 * otherwise returns unchanged one
 * @returns string with env settings
 */
function assignIfNotNull(valueToChange, envFileData) {
  let value;
  if (envFileData === undefined || envFileData === null || envFileData === '' || envFileData === 'ENV_FILE_NOT_AVAILABLE') {
    value = valueToChange;
  } else {
    value = envFileData;
  }
  return value;
}

/**
 * Gets test data settings from config/testdata/data-settings.json
 * @returns json object with data settings
 */
function getAllTestDataSettings() {
  let ls = propreader.getDataSettings();
  return ls;
}

// EXPORTED FUNCTIONS AND VALUES

/**
 * Gets location where this framework sits
 * @returns file path
 */
export function getTafPath() {
  let p = path.resolve(__dirname);
  return p;
}

export let configuredEnv = getCurrentEnvDetails(getCurrentEnv());
export let configuredUsers = getUsersForEnv(getCurrentEnv());
export let mailHost = getMailHost();
export let standardPassword = getStandardPassword();
export let standardPasswordAlt = getStandardPasswordAlt();
export let loggerSettings = getLoggerSettings();
export let configuredTestData = getAllTestDataSettings();
