'use strict';

import { strict as assert } from 'assert';
import logger from '../../helpers/logger.js';
import * as testlab from '../../testlab/testlab.js';
import * as suite from './suiteconfig.js';
import jp from 'jsonpath';

describe('dummy suite', function(){

  this.timeout(testlab.mochaTimeout);
  let suiteId = 'dummy';
  logger.info('Current environment is: ' + testlab.currEnv.name);
  
  describe('Testing /data/v1/user', function(){

    describe('case001: Verify that if APP_ID is missing, an error message is given out', function(){

      let caseId = 'case001'; 
      logger.info('Running test case ' + caseId + ' of suite ' + suiteId);

      let testCaseData = suite.getCaseDataFull(suiteId, caseId);
      logger.debug('Default test data settings are as follows: ' + suite.getCaseDataFullAsPrettyString(suiteId, caseId));
      logger.debug('Please note, that request data may be overwritten at request configuration step, please read futher');

      let parameters = '?limit=' + testlab.currTestData.users.query.limit;

      let responseRaw, responseJson;

      before( async function() {

        responseRaw = await testlab.makeRequest(
          testCaseData.request.path,
//          testCaseData.request.parameters,
          parameters,
          testCaseData.request.method, 
          testCaseData.request.headers, 
          testCaseData.request.body);
        responseJson = await responseRaw.json();

        logger.debug('Actual response: ' + testlab.getResposeAsPrettyString(responseJson));

      });

      it('expecting response status to be 403', async function(){
        let actualValue = await responseRaw.status;
        logger.debug('Actual response status is: ' + JSON.stringify(actualValue));
        assert.strictEqual(actualValue, 403);
      });

      it('expecting to receive an error message: APP_ID_MISSING', async function(){
        let actualValue = jp.query(responseJson, '$["error"]').pop();
        logger.debug('Actual message is : ' + actualValue);
        assert.strictEqual(actualValue, 'APP_ID_MISSING');
      });

    });

  });

});