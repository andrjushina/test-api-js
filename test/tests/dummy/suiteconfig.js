'use strict';

import jp from 'jsonpath';
import path from 'path';
import fs from 'fs';
import logger from '../../helpers/logger.js';
import * as p from '../../config/propreader.js';

const __dirname = path.resolve();

function getAllForSuite(suiteName){
  let all_values = fs.readFileSync(
    path.resolve(__dirname, p.configureSuitePath(suiteName).suiteConfigPath), 
    p.configureSuitePath(suiteName).suiteConfigFileEncoding);
  return all_values;
}

// Suite-specific settings

export function getMochaTimeoutForSuite(suiteName){
  let all_data = getAllForSuite(suiteName);
  logger.silly('Full suite data: ' + all_data);
  let value = jp.query(JSON.parse(all_data), '$.' + suiteName + '.mocha_timeout_suite').pop();
  return value;
}

// Get information about a test case - short

export function getCaseSummary(suiteName, caseId){
  let all_data = getAllForSuite(suiteName);
  logger.silly('Full suite data: ' + all_data);
  let value = jp.query(JSON.parse(all_data), '$.' + suiteName + '.cases.'+ caseId + '.summary' ).pop();
  return value;
}

export function getCaseSummaryAsPrettyString(suiteName, caseId){
  return JSON.stringify(getCaseSummary(suiteName, caseId), null, 2);
}

// Get information about a test case - full

export function getCaseDataFull(suiteName, caseId){
  let all_data = getAllForSuite(suiteName);
  logger.silly('Full suite data: ' + all_data);
  let value = jp.query(JSON.parse(all_data), '$.' + suiteName + '.cases.'+ caseId ).pop();
  return value;
}

export function getCaseDataFullAsPrettyString(suiteName, caseId){
  return JSON.stringify(getCaseDataFull(suiteName, caseId), null, 2);
}