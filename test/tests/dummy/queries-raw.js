'use strict';

import logger from '../../helpers/logger.js';

export async function selectAllPosts(client, userActorId) {
  let query;
  try {
    query = await client.query("\
    SELECT p.post_id \
    FROM posts p \
    WHERE p.deleted_at IS NULL \
    AND p.hidden_at IS NULL \
    AND p.user_id not in \
    (SELECT DISTINCT ub.blocker_user_id \
    FROM user_blocks ub \
    WHERE ub.blockee_user_id = '" + userActorId + "')");
  } catch (e) {
    logger.error(e);
  }
  logger.silly('Query: ' + JSON.stringify(query));
  return query;
}

export async function selectAllPostsWithOutComments(client, userActorId) {
  let query;
  try {
    query = await client.query("\
    SELECT p.post_id \
    FROM posts p \
    WHERE p.deleted_at IS NULL \
    AND p.hidden_at IS NULL \
    AND p.user_id not in \
    (SELECT DISTINCT ub.blocker_user_id \
    FROM user_blocks ub \
    WHERE ub.blockee_user_id = '" + userActorId + "')\
    EXCEPT \
    SELECT DISTINCT c.post_id \
    FROM comments c \
    WHERE c.post_id IS NOT NULL\
    AND c.deleted_at IS NULL");
  } catch (e) {
    logger.error(e);
  }
  logger.silly('Query: ' + JSON.stringify(query));
  return query;
}

export async function selectAllPostsWithComments(client, userActorId) {
  let query;
  try {
    query = await client.query("\
    SELECT p.post_id \
    FROM posts p \
    WHERE p.deleted_at IS NULL \
    AND p.hidden_at IS NULL \
    AND p.user_id not in \
    (SELECT DISTINCT ub.blocker_user_id \
    FROM user_blocks ub \
    WHERE ub.blockee_user_id = '" + userActorId + "')\
    INTERSECT \
    SELECT DISTINCT c.post_id \
    FROM comments c \
    WHERE c.post_id is NOT NULL\
    AND c.deleted_at IS NULL");
  } catch (e) {
    logger.error(e);
  }
  logger.silly('Query: ' + JSON.stringify(query));
  return query;
}