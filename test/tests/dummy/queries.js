'use strict';

import logger from '../../helpers/logger.js';
import * as qh from '../../helpers/db/queries-handlers.js';
import * as qr from './queries-raw.js';

// posts

// all posts neither hidden nor deleted
export async function getRandomPost(client, userId) {
  let resultSet = await qr.selectAllPosts(client, userId);
  let selectedPost = qh.getRandomValueFromResultSet(resultSet, 'post_id');
  logger.silly('Selected post id: ' + selectedPost);
  return selectedPost;
}

// all hidden posts (not deleted)
export async function getRandomHiddenPost(client, userId) {
  let resultSet = await qr.selectAllPostsThatAreHidden(client, userId);
  let selectedPost = qh.getRandomValueFromResultSet(resultSet, 'post_id');
  logger.silly('Selected post id: ' + selectedPost);
  return selectedPost;
}

// posts with comments neither hidden nor deleted
export async function getRandomPostWithComments(client, userId) {
  let resultSet = await qr.selectAllPostsWithComments(client, userId);
  let selectedPost = qh.getRandomValueFromResultSet(resultSet, 'post_id');
  logger.silly('Selected post id: ' + selectedPost);
  return selectedPost;
}

// posts without comments neither hidden nor deleted
export async function getRandomPostWithOutComments(client, userId) {
  let resultSet = await qr.selectAllPostsWithOutComments(client, userId);
  let selectedPost = qh.getRandomValueFromResultSet(resultSet, 'post_id');
  logger.silly('Selected post id: ' + selectedPost);
  return selectedPost;
}