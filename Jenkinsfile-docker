pipeline {
  agent any
  triggers {
      githubPush()
  }
  environment {
    GIT_CREDS_ID='app-git'
    GIT_URL='git@gitlab.com:andrjushina/test-api-js.git'
    AWS_REGION='eu-west-1'
    AWS_DOCKER_REPO_USER='AWS'
    REG_HOST = 'blahblablah.dkr.ecr.eu-west-1.amazonaws.com'
    AWS_CREDENTIAL_ID='jenkins'
    PROJECT_NAME= 'test-api-js'
    IMAGE_TAG = getImageTag(params.BRANCH)
    IMAGE_NAME_FULL="${REG_HOST}/${PROJECT_NAME}:${IMAGE_TAG}"
    SECRET_FILE_ID = credentials('env-file-test-api')
  }
  stages {
    stage('Get source code') {
      steps {
        script {
          echo "Checking out project source code"
          checkout([
            $class: 'GitSCM',
            branches: [
                [name: params.BRANCH ]
            ],
            doGenerateSubmoduleConfigurations: false,
            extensions: [],
            submoduleCfg: [],
            userRemoteConfigs: [
              [
                  credentialsId: 'app-git',
                  url: 'git@gitlab.com:andrjushina/test-api-js.git'
              ]
            ]
          ])
        }
      }
    }
    stage('Getting .env file from Jenkins secrets') {
      steps{
        sh('rm .env 2> /dev/null || true')
        sh('cp -v ${SECRET_FILE_ID} .')
        sh('stat .env')
      }
    }
    stage('Checking state of environment') {
      steps{
        sh('echo "Image registry host: ${REG_HOST}"')
        sh('echo "Project name: ${PROJECT_NAME}"')
        sh('echo "Image tag value: ${IMAGE_TAG}"')
      }
    }
    stage('Building and tagging the image') {
      steps{
        sh('echo "Building image tag using the following value for the tag: ${IMAGE_TAG}"')
        sh('docker build --tag ${IMAGE_NAME_FULL} -f Dockerfile .')
      }
    }
    stage('Deploying Image') {
      steps{
        withCredentials([[$class: 'AmazonWebServicesCredentialsBinding', accessKeyVariable: "AWS_ACCESS_KEY_ID", credentialsId: "${AWS_CREDENTIAL_ID}", secretKeyVariable: "AWS_SECRET_ACCESS_KEY"]]){
          sh('aws ecr get-login-password --region ${AWS_REGION} | docker login -u ${AWS_DOCKER_REPO_USER} --password-stdin ${REG_HOST}')
          sh('docker image push ${IMAGE_NAME_FULL}')
        }
      }
    }
  }
  post {
    always {
      sh('echo "Cleaning local data"')
      sh('echo "Local image with the name of ${IMAGE_NAME_FULL} is about to be removed"')
      sh('docker image rm ${IMAGE_NAME_FULL}')
      sh('echo "Removing .env file"')
      sh('rm .env 2> /dev/null || true')
      sh('echo "Workspace is about to be cleaned out"')
      cleanWs()
    }
  }
}


def getImageTag(branchName){
  def tag
  switch (branchName){
    case ~/.*release.*/:
      tag = 'uat' + '-latest'
      println "${tag}"
      break;
    case ~/.*develop.*/:
      tag = 'int' + '-latest'
      println "${tag}"
      break;
    case ~/.*feature.*/:
      tag = 'fea' + '-latest-' + env.GIT_COMMIT.substring(0,9)
      println "${tag}"
      break;
    case ~/.*fix.*/:
      tag = 'fix' + '-latest-' + env.GIT_COMMIT.substring(0,9)
      println "${tag}"
      break;
    case ~/.*refactoring.*/:
      tag = 'ref' + '-latest-' + env.GIT_COMMIT.substring(0,9)
      println "${tag}"
      break;
    default:
      tag = 'invalid'
      println "${tag}"
  }
  return tag
}
