pipeline {
  agent any
  environment {
    GIT_BRANCH=getGitBranch()
    GIT_CREDS_ID='app-git'
    GIT_URL='git@gitlab.com:andrjushina/test-api-js.git'
    AWS_REGION='eu-west-1'
    AWS_DOCKER_REPO_USER='AWS'
    REG_HOST='259366225118.dkr.ecr.eu-west-1.amazonaws.com'
    AWS_CREDENTIAL_ID='jenkins'
    PROJECT_NAME='test-api'
    IMAGE_TAG=getImageTagFromEnv(params.BACKEND_ENV)
    IMAGE_NAME_FULL="${REG_HOST}/${PROJECT_NAME}:${IMAGE_TAG}"
    TEST_SCOPE=getTestScope(params.TEST_SCOPE)
    CONTAINER_ID=''
    NETWORK_NAME='testapinet'
  }
  stages {
    stage('Get source code') {
      steps {
        script {
          echo "Checking out project source code"
          checkout([
            $class: 'GitSCM',
            branches: [
                [name: "${GIT_BRANCH}" ]
            ],
            doGenerateSubmoduleConfigurations: false,
            extensions: [],
            submoduleCfg: [],
            userRemoteConfigs: [
              [
                  credentialsId: "${GIT_CREDS_ID}",
                  url: "${GIT_URL}"
              ]
            ]
          ])
        }
      }
    }
    stage('Logging in to AWS ECR') {
      steps{
        withCredentials( [[$class: 'AmazonWebServicesCredentialsBinding', credentialsId: "${AWS_CREDENTIAL_ID}", accessKeyVariable: "AWS_ACCESS_KEY_ID",  secretKeyVariable: "AWS_SECRET_ACCESS_KEY"]] )
          {
            sh('aws ecr get-login-password --region ${AWS_REGION} | docker login -u ${AWS_DOCKER_REPO_USER} --password-stdin ${REG_HOST}')
          }
      }
    }
    stage('Pulling the image from AWS ECR') {
      steps{
        sh('docker pull ${IMAGE_NAME_FULL}')
      }
    }
    stage('Starting container') {
      steps{
        sh('rm -rf "\$(pwd)/logs"')
        sh('mkdir "\$(pwd)/logs"')
        sh('docker network ls')
        sh('docker network create --driver=bridge --subnet=172.28.0.0/16 ${NETWORK_NAME}')
        sh('docker run -dit --name testapi --network="${NETWORK_NAME}" --mount type=bind,source="\$(pwd)"/logs,target=/app/logs ${IMAGE_NAME_FULL}')
      }
    }
    stage('Running tests') {
      steps{
        // commented out part returns 'Bad substitution', keeping it here for historical purposes, do not remove
        //sh('docker exec --env BACKEND_ENV=${params.BACKEND_ENV} \$(docker ps -q --filter ancestor=${IMAGE_NAME_FULL}) ls -laht')
        //sh('docker exec --env BACKEND_ENV=${params.BACKEND_ENV} \$(docker ps -q --filter ancestor=${IMAGE_NAME_FULL}) npm run ${env.TEST_SCOPE}')
        sh """
          docker exec --env BACKEND_ENV=${params.BACKEND_ENV} \$(docker ps -q --filter ancestor=${IMAGE_NAME_FULL}) ls -laht
          docker exec --env BACKEND_ENV=${params.BACKEND_ENV} \$(docker ps -q --filter ancestor=${IMAGE_NAME_FULL}) npm run ${env.TEST_SCOPE}
        """
      }
      post {
        always {
          archiveArtifacts artifacts: 'logs/*'
        }
      }
    }
  }
  post {
    always {
      sh('echo "Cleaning local data"')
      sh('echo "Removing custom networks"')
      sh('docker network disconnect -f ${NETWORK_NAME} \$(docker ps --filter ancestor=${IMAGE_NAME_FULL} -q)')
      sh('docker network rm ${NETWORK_NAME}')
      sh('echo "Local container is about to be killed and removed"')
      sh('docker rm -f \$(docker ps --filter ancestor=${IMAGE_NAME_FULL} -q)')
      sh('docker image rm ${IMAGE_NAME_FULL}')
      sh('echo "Workspace is about to be cleaned out"')
      cleanWs()
    }
  }
}

// not to be confused with a similar method in docker jenkinsfile
// not sure which one is better: to take hash from params
// or assume that exec immediately follows docker and take them from env.GIT_COMMIT
// please do not removed the commented out part
def getImageTagFromEnv(backendEnv){
  def tag
  switch (backendEnv){
    case ~/.*UAT.*/:
      tag = 'uat' + '-latest'
      println "${tag}"
      break;
    case ~/.*INT.*/:
      tag = 'int' + '-latest'
      println "${tag}"
      break;
    case ~/.*FEA.*/:
      if (params.GIT_COMMIT_HASH == 'enter_new_value') {
        tag = 'fea' + '-latest-' + env.GIT_COMMIT.substring(0,9)
        } else {
        tag = 'fea' + '-latest-' + params.GIT_COMMIT_HASH
        }
      println "${tag}"
      break;
    case ~/.*FIX.*/:
      if (params.GIT_COMMIT_HASH == 'enter_new_value') {
        tag = 'fix' + '-latest-' + env.GIT_COMMIT.substring(0,9)
        } else {
        tag = 'fix' + '-latest-' + params.GIT_COMMIT_HASH
        }
      println "${tag}"
      break;
    case ~/.*REF.*/:
      if (params.GIT_COMMIT_HASH == 'enter_new_value') {
        tag = 'ref' + '-latest-' + env.GIT_COMMIT.substring(0,9)
        } else {
        tag = 'ref' + '-latest-' + params.GIT_COMMIT_HASH
        }
      println "${tag}"
      break;
    default:
      tag = 'invalid'
      println "${tag}"
  }
  return tag
}

def getTestScope(testScope){
  def scopeMap = [
    ALL: "all",
    DUMMY: "dummy",
    DRAFTS: "drafts"
  ]
  def scope = scopeMap.get(params.TEST_SCOPE)
  return scope
}

def getGitBranch(){
  return params.BRANCH
}
