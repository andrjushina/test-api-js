FROM node:20
RUN mkdir -p /app/test-api-js
WORKDIR /app/test-api-js
COPY . /app/test-api-js
RUN npm install
RUN mkdir logs
RUN mkdir shared